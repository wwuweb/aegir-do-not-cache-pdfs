<?php

/**
 * @file
 * Custom caching policies for specific filetypes.
 */

/**
 * Implements hook_provision_apache_vhost_config().
 */
function donotcachepdfs_provision_apache_vhost_config($uri, $data) {
  if ($uri === 'cse.wwu.edu' || preg_match('@^cse\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $headers[] = '';

    $headers[] = '<IfModule mod_headers.c>';
    $headers[] = '  <FilesMatch "(?i)^.*\.pdf$">';
    $headers[] = '    Header set Cache-Control "no-cache, no-store"';
    $headers[] = '  </FilesMatch>';
    $headers[] = '</IfModule>';

    $headers[] = '';

    return $headers;
  }

  if ($uri === 'budgetoffice.wwu.edu' || preg_match('@^budgetoffice\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $headers[] = '';

    $headers[] = '<IfModule mod_headers.c>';
    $headers[] = '  <FilesMatch "(?i)^.*\.pdf$">';
    $headers[] = '    Header set Cache-Control "no-cache, no-store"';
    $headers[] = '  </FilesMatch>';
    $headers[] = '</IfModule>';

    $headers[] = '';

    return $headers;
  }

  if ($uri === 'wce.wwu.edu' || preg_match('@^wce\dx\d+\.wwu\.edu$@', $uri) === 1) {
    $headers[] = '';

    $headers[] = '<IfModule mod_headers.c>';
    $headers[] = '  <FilesMatch "(?i)^.*\.pdf$">';
    $headers[] = '    Header set Cache-Control "no-cache, no-store"';
    $headers[] = '  </FilesMatch>';
    $headers[] = '</IfModule>';

    $headers[] = '';

    return $headers;
  }
}
